#!/bin/bash
backup_dir='' # Emplacement des backup
backup_name='' # Nom du backup
compression='lz4'

sshkey_back='' # Emplacement de la key de connection ssh
server_back='@:' # Definition du serveur et emplacement USER@SERVEUR:PATH
sshport_back='22' # Port du serveur
file_back='' # Point de montage du backup

log='/tmp/borg_script.log' # Emplacement du log en cas d'erreur

err()
{
    echo "FATAL ERROR: ${1}"
    [[ -n $log ]] && echo "[$(date)] FATAL ERROR: ${1}" >> "$log"

    exit 1
}

mkdir -p "$file_back" || err "mkdir fail to create \"$file_back\""
mkdir -p "$backup_dir" || err "mkdir fail to create \"$backup_dir\""

sshfs -p "$sshport_back" -oIdentityFile="$sshkey_back" "$server_back" "$file_back" || err "fail to mount \"$server_back\""

borg list "$backup_dir" || borg init --encryption none "$backup_dir"
borg create -C "$compression" "$backup_dir::${backup_name}__{now:%d.%m.%Y}" "$file_back"
borg list "$backup_dir"
borg prune -v --keep-within=14d --keep-weekly=4 --keep-monthly=-2 "$backup_dir"

fusermount -u "$file_back"